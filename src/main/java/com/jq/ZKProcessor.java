package com.jq;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import javafx.scene.control.TreeItem;
import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;

import java.util.ArrayList;
import java.util.List;

public class ZKProcessor {

    String url;

    public ZKProcessor(String url) {
        this.url = url;
    }

    /**
     * 获取zk客户端
     *
     * @return
     */
    public CuratorFramework getClient() {
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
        CuratorFramework client = CuratorFrameworkFactory.builder()
                .connectString(url)
                .sessionTimeoutMs(5000)  // 会话超时时间
                .connectionTimeoutMs(5000) // 连接超时时间
                .retryPolicy(retryPolicy)
                .build();
        client.start();
        return client;
    }

    //获取zk一个节点下的所有子孙节点
    public List<JSONObject> getAllChildren(CuratorFramework client, String path) throws Exception {

        List<JSONObject> list = new ArrayList<>();
        List<String> children = client.getChildren().forPath(path);
        for (String s : children) {
            JSONObject obj = new JSONObject(true);
            obj.put("name", s);
            String thisPath = path + (path.equals("/") ? "" : "/") + s;
            obj.put("path", thisPath);
            obj.put("value", new String(client.getData().forPath(thisPath)));

            List<JSONObject> grandChildren = getAllChildren(client, thisPath);
            if (grandChildren.size() > 0)
                obj.put("children", grandChildren);
            list.add(obj);
        }
        return list;

    }

    //获取zk上的所有数据，返回json string
    public String getAllNode() {
        CuratorFramework client = getClient();
        List<JSONObject> children = null;
        try {
            children = getAllChildren(client, "/");
            String s = JSON.toJSONString(children);
            return s;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //删除zk上的某个节点
    public void removeNode(String path) {
        try {
            CuratorFramework client = getClient();
            client.delete().guaranteed().deletingChildrenIfNeeded().forPath(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getValue(String path) {
        System.out.println(path);
        CuratorFramework client = getClient();
        try {
            byte[] bytes = client.getData().forPath(path);
            return new String(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //查询zk上的所有数据，封装成treeView的格式
    public TreeItem<ZKNode> getTreeItem() {
        String s = getAllNode();
        JSONArray jsonArray = JSON.parseArray(s);
        ZKNode zkNode = new ZKNode("/", "/", "");
        TreeItem<ZKNode> root = new TreeItem<>(zkNode);
        root.setExpanded(true);
        get(jsonArray, root);
        return root;
    }

    //递归调用
    public void get(JSONArray jsonArray, TreeItem<ZKNode> root) {
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String name = jsonObject.getString("name");
            String path = jsonObject.getString("path");
            String value = jsonObject.getString("value");
            ZKNode node = new ZKNode(name, path, value);
            TreeItem<ZKNode> item = new TreeItem<>(node);

            item.setExpanded(true);
            JSONArray children = jsonObject.getJSONArray("children");
            if (children != null && children.size() > 0) {
                get(children, item);
            }
            root.getChildren().add(item);
        }
    }

    public static void main(String[] args) throws Exception {
        ZKProcessor zkProcessor = new ZKProcessor("localhost:2181");
        CuratorFramework client = zkProcessor.getClient();
        byte[] bytes = client.getData().forPath("/dolphinscheduler/tasks_queue/2_8_2_8_3232239617");
        String string = new String(bytes);
        System.out.println(string);
        JSONObject obj = new JSONObject();
        obj.put("value", bytes);
        System.out.println(obj.toJSONString());

    }

}
