package com.jq.conf;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class Connection {

    String name;
    String url;

    @FXML
    StringProperty nameProperty;
    @FXML
    StringProperty urlProperty;


    public Connection(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public Connection() {

    }


    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public StringProperty getNameProperty() {
        return nameProperty;
    }

    public StringProperty getUrlProperty() {
        return urlProperty;
    }

    public void setName(String name) {
        this.name = name;
        this.nameProperty = new SimpleStringProperty(name);
    }

    public void setUrl(String url) {
        this.url = url;
        this.urlProperty = new SimpleStringProperty(url);
    }
}
