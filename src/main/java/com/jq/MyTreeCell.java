package com.jq;

import javafx.scene.control.TreeCell;

public class MyTreeCell extends TreeCell<ZKNode> {

    @Override
    protected void updateItem(ZKNode item, boolean empty) {
        super.updateItem(item, empty);
        if (item == null) {
            this.setText("");
        } else {
            this.setText(item.getName());
        }
    }
}
