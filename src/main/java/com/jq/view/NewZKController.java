package com.jq.view;

import com.jq.util.ConfUtil;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class NewZKController {

    @FXML
    private TextField name;
    @FXML
    private TextField url;

    private Stage stage;

    @FXML
    public void initialize() {
    }

    @FXML
    public void create() {
        String name = this.name.getText();
        String url = this.url.getText();
        ConfUtil.save(name, url);
        this.stage.close();
    }

    @FXML
    public void createAndCollect() {

    }


    public void setStage(Stage stage) {
        this.stage = stage;
    }


}
