package com.jq.view;

import com.jq.Main;
import com.jq.MyTreeCell;
import com.jq.ZKProcessor;
import com.jq.ZKNode;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

public class RootController {

    Stage stage;
    String zkUrl;

    @FXML
    TreeView<ZKNode> tv;

    @FXML
    private Label name;
    @FXML
    private Label path;
    @FXML
    private Label value;

    @FXML
    private void initialize() {

        //treeView显示值的设置
        tv.setCellFactory(new Callback<TreeView<ZKNode>, TreeCell<ZKNode>>() {
            @Override
            public TreeCell<ZKNode> call(TreeView<ZKNode> param) {
                return new MyTreeCell();
            }
        });

        //选中节点的事件监听
        tv.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                ZKNode node = newValue.getValue();
                name.setText(node.getName());
                path.setText(node.getPath());
                value.setText(node.getValue());
            } else {
                name.setText("");
                path.setText("");
                value.setText("");
            }
        });
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void initZKData() {
        ZKProcessor ZKProcessor = new ZKProcessor(zkUrl);
        TreeItem<ZKNode> treeItem = ZKProcessor.getTreeItem();
        this.tv.setRoot(treeItem);
    }

    @FXML
    public void refreshAll() {
        initZKData();
    }

    @FXML
    public void refreshNode() {
        ZKProcessor ZKProcessor = new ZKProcessor(zkUrl);
        String value = ZKProcessor.getValue(this.path.getText());
        this.value.setText(value);
    }

    @FXML
    public void deleteNode() {
        ZKProcessor ZKProcessor = new ZKProcessor(zkUrl);
        ZKProcessor.removeNode(this.path.getText());
        refreshAll();
    }

    /**
     * 打开连接窗口
     */
    @FXML
    public void openConnect() {
        System.out.println("----------------------");
        try {

            FXMLLoader loader = new FXMLLoader();

            loader.setLocation(getClass().getResource("StartConnect.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            Stage newStage = new Stage();
            newStage.setTitle("开始连接");
            newStage.initModality(Modality.WINDOW_MODAL);
            newStage.initOwner(stage);
            Scene scene = new Scene(page);
            newStage.setScene(scene);

            StartConnectController controller = loader.getController();
            controller.setStage(newStage);
            controller.setRootController(this);

            newStage.showAndWait();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 打开创建连接窗口
     */
    @FXML
    public void openCreateUrl() {
        System.out.println(1234);
        try {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("NewZkConf.fxml"));
            AnchorPane page = (AnchorPane) loader.load();


            // Create the dialog Stage.
            Stage newStage = new Stage();
            newStage.setTitle("添加新的连接");
            newStage.initModality(Modality.WINDOW_MODAL);
            newStage.initOwner(stage);
            Scene scene = new Scene(page);
            newStage.setScene(scene);

            NewZKController controller = loader.getController();

            controller.setStage(newStage);

            newStage.showAndWait();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setZkUrl(String url) {
        this.zkUrl = url;
    }
}
